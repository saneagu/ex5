public class Hotel_Bedrooms {

    private int dimension, height, weight;
    private String style;

    Hotel_Bedrooms(int dimension, int height, int weight, String style) {
        this.dimension = dimension;
        this.height = height;
        this.weight = weight;
        this.style = style;
    }

    public String toString() {
        return "The style of this bed is called" + getStyle();
    }

    public String getStyle() {
        return style;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int CalculateSize(){
        return getDimension() + getHeight() + getWeight();
    }
}